# Software License Agreement (BSD License)
#
# Copyright (c) 2012, Willow Garage, Inc.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of Willow Garage, Inc. nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import rospy
import tf
from tf.transformations import quaternion_from_euler

import numpy
import random
from math import sqrt, atan, pi, degrees, floor
from sensor_msgs.msg import Image
from nav_msgs.msg import OccupancyGrid, Path, Odometry
from geometry_msgs.msg import PolygonStamped, PointStamped, PoseWithCovarianceStamped, PoseStamped

from python_qt_binding.QtCore import Signal, Slot, QPointF, qWarning, Qt, SIGNAL
from python_qt_binding.QtCore import *
from python_qt_binding.QtGui import QWidget, QPixmap, QImage, QGraphicsView, QGraphicsScene, QPainterPath, QPen, QPolygonF, QVBoxLayout, QHBoxLayout, QColor, qRgb, QPushButton, QLabel, QLineEdit
from python_qt_binding.QtGui import *

from rqt_py_common.topic_helpers import get_field_type

import RobotIcon
import ObjectIcon
import os, csv
import rospkg

def accepted_topic(topic):
    msg_types = [OccupancyGrid, Path, PolygonStamped, PointStamped]
    msg_type, array = get_field_type(topic)

    if not array and msg_type in msg_types:
        return True
    else:
        return False

class HRIGetPosWidget(QWidget):

    def __init__(self, map_topic='/map'):
        super(HRIGetPosWidget, self).__init__()
        self._layout = QVBoxLayout()
        self._h_layout = QHBoxLayout()
        self.setAcceptDrops(True)
        self.setWindowTitle('HRI Navigation GetPose')

        self.map = map_topic
        self._tf = tf.TransformListener()

        
      


        hNavLayout = QHBoxLayout()
        vVidLayout = QVBoxLayout()

     
        self._map_view = HRIGetPos(map_topic, tf = self._tf, parent = self)
        self._wordBank = HRIWordBank(parent = self)
        self._doneButton = QPushButton('Done!')
        self._doneButton.clicked.connect(self._map_view.savePoses)
        
        #Add the word bank to the left side
        hNavLayout.addWidget(self._wordBank)
        hNavLayout.addWidget(self._map_view)

        
        self._layout.addLayout(hNavLayout)
        self._layout.addWidget(self._doneButton)
        self.setLayout(self._layout)

        
    def dragEnterEvent(self, e):
        if not e.mimeData().hasText():
            if not hasattr(e.source(), 'selectedItems') or len(e.source().selectedItems()) == 0:
                qWarning('HRIGetPos.dragEnterEvent(): not hasattr(event.source(), selectedItems) or len(event.source().selectedItems()) == 0')
                return
            item = e.source().selectedItems()[0]
            topic_name = item.data(0, Qt.UserRole)
            if topic_name == None:
                qWarning('HRIGetPos.dragEnterEvent(): not hasattr(item, ros_topic_name_)')
                return

        else:
            topic_name = str(e.mimeData().text())

        if accepted_topic(topic_name):
            e.accept()
            e.acceptProposedAction()

    def dropEvent(self, e):
        if e.mimeData().hasText():
            topic_name = str(e.mimeData().text())
        else:
            droped_item = e.source().selectedItems()[0]
            topic_name = str(droped_item.data(0, Qt.UserRole))

        topic_type, array = get_field_type(topic_name)

        #Add the word bank icon to the map at the desired position

    def save_settings(self, plugin_settings, instance_settings):
        self._map_view.save_settings(plugin_settings, instance_settings)

    def restore_settings(self, plugin_settings, instance_settings):
        self._map_view.restore_settings(plugin_settings, instance_settings)

        
class HRIWordBank(QGraphicsView):
    #Make a set of object icons and arrange them vertically in a canvas

    def __init__(self, parent=None):
        super(HRIWordBank,self).__init__()
        self._parent = parent
        
        self._scene = QGraphicsScene()
        self.setScene(self._scene)
        self.w = 0
        self.h = 0
        self.setMaximumWidth(100)
        self._models = dict()
        
        rospack = rospkg.RosPack()
        print 'Working dir:' + rospack.get_path('ramrod')
        self.addModelPictures(rospack.get_path('ramrod'))
        self._currentItem = None
        
    def addModelPictures(self, srcPath):
        modelNames = open(srcPath + '/models/gazebo_models.csv','r')
        reader = csv.reader(modelNames)
        next(reader, None) #skip header
        curY = 0
        for row in reader:
            #For each model name:
            #load the image into a pixmap
            #assign to scene
            print 'Loading object:' + row[0]
  
            thePM = ObjectIcon.ObjectIcon(srcPath + '/hci_objects/', row[0])
            self._scene.addItem(thePM)
            self._models[row[0]] = thePM
    
            thePM.setPos(QPointF(0, curY))
            curY = curY + 100

        self.w = 100
        self.h = curY
        self.setSceneRect(0, 0, self.w, self.h)
        return
    
    def mousePressEvent(self,e):

        p = self.mapToScene(e.x(), e.y())
        objectSel = self._scene.itemAt(QPointF(p.x(), p.y()))

        if isinstance(objectSel, ObjectIcon.ObjectIcon):
            if self._currentItem:
                self._currentItem.selected(False)

            #objectSel.getPixelRGB(p.x(), p.y())
            objectSel.selected(True)
            self._currentItem = objectSel
            
    def resizeEvent(self, evt=None):
        #Resize

        bounds = self._scene.itemsBoundingRect()

        if bounds:
            #Wait until bounds exist, which only happen after the map is updated once
            bounds.setWidth(bounds.width()*1.0)         
            bounds.setHeight(bounds.height()*1.0)
            self.fitInView(bounds, Qt.KeepAspectRatio)
            #self.centerOn(self._imageGI)
            self.show()
            
class HRIGetPos(QGraphicsView):
    map_changed = Signal()
   
    
    def __init__(self, map_topic='/map',
                 tf=None, parent=None):
        super(HRIGetPos, self).__init__()
        self._parent = parent

        self._goal_mode = True

        self.map_changed.connect(self._updateMap)
       
        
        self.setDragMode(QGraphicsView.NoDrag)

        self._map = None
        self._map_item = None
        self._addedItems = dict()
        self.w = 0
        self.h = 0

        self._colors = [(238, 34, 116), (68, 134, 252), (236, 228, 46), (102, 224, 18), (242, 156, 6), (240, 64, 10), (196, 30, 250)]
        self._scene = QGraphicsScene()

       
        self.map_sub = rospy.Subscriber('/map', OccupancyGrid, self.map_cb)

        self.setScene(self._scene)

    def add_dragdrop(self, item):
        # Add drag and drop functionality to all the items in the view
        def c(x, e):
            self.dragEnterEvent(e)
        def d(x, e):
            self.dropEvent(e)
        item.setAcceptDrops(True)
        item.dragEnterEvent = c
        item.dropEvent = d

    def dragEnterEvent(self, e):
        print 'map enter event'

    def dropEvent(self, e):
        print 'Nav drop event'
            
    def mousePressEvent(self,e):

        p = self.mapToScene(e.x(), e.y())
        if self._parent._wordBank._currentItem:
            modelName = self._parent._wordBank._currentItem._modelName
            wordItem = self._parent._wordBank._currentItem
            if self._addedItems.has_key(modelName):
                self._scene.removeItem(self._addedItems[modelName])
                
            #Draw a pixmap of the current item on the map
            newPM = ObjectIcon.ObjectIcon(None, None, wordItem)
            self._addedItems[modelName] = newPM
            self._scene.addItem(newPM)
            #newPM.rescale(1)
            newPM.setPos(QPointF(p.x() - 50, p.y() - 50))

    def savePoses(self, msg):
        #Save the position of each of the pixmaps for the selected objects
        #Also save object names
        rospack = rospkg.RosPack()
        print 'Working dir:' + rospack.get_path('ramrod')

        outFile = open(rospack.get_path('ramrod') + '/user_output/model_pos.txt','w')
        
        for model in self._addedItems:
            xCoord = (3100 + self._addedItems[model].x() + 50)/100
            yCoord = (3100 - self._addedItems[model].y() + 50)/100
            outFile.write(model + ',' + str(xCoord) + ',' + str(yCoord) + '\n')
        outFile.close()    
            
    def map_cb(self, msg):
        self.resolution = msg.info.resolution
        self.w = msg.info.width
        self.h = msg.info.height

        a = numpy.array(msg.data, dtype=numpy.uint8, copy=False, order='C')
        a = a.reshape((self.h, self.w))
        if self.w % 4:
            e = numpy.empty((self.h, 4 - self.w % 4), dtype=a.dtype, order='C')
            a = numpy.append(a, e, axis=1)
        image = QImage(a.reshape((a.shape[0] * a.shape[1])), self.w, self.h, QImage.Format_Indexed8)

        for i in reversed(range(101)):
            image.setColor(100 - i, qRgb(i* 2.55, i * 2.55, i * 2.55))
        image.setColor(101, qRgb(255, 0, 0))  # not used indices
        image.setColor(255, qRgb(200, 200, 200))  # color for unknown value -1
        self._map = image
        
        self.map_changed.emit()

    def close(self):
        if self.map_sub:
            self.map_sub.unregister()
        super(HRIGetPos, self).close()
        
    def dragMoveEvent(self, e):
        print('Scene got drag move event')
        
    def resizeEvent(self, evt=None):
        #Resize map to fill window
        bounds = self._scene.sceneRect()
        if bounds:
            self.fitInView(self._scene.sceneRect(), Qt.KeepAspectRatio)
            self.centerOn(self._map_item)
            self.show()
 
    def _updateMap(self):
        if self._map_item:
            self._scene.removeItem(self._map_item)

        pixmap = QPixmap.fromImage(self._map)
        self._map_item = self._scene.addPixmap(pixmap.scaled(self.w*100,self.h*100))
        self._map_item.setPos(QPointF(0, 0))
        # Everything must be mirrored
        self._mirror(self._map_item)

        # Add drag and drop functionality
        self.add_dragdrop(self._map_item)

        #Resize map to fill window
        self.setSceneRect(-self.w*100, 0, self.w*100, self.h*100)

        self.fitInView(self._scene.sceneRect(), Qt.KeepAspectRatio)
        self.centerOn(self._map_item)
        self.show()


    def _mirror(self, item):
        item.scale(-1, 1)
        item.translate(-self.w, 0)

    def save_settings(self, plugin_settings, instance_settings):
        # TODO add any settings to be saved
        pass

    def restore_settings(self, plugin_settings, instance_settings):
        # TODO add any settings to be restored
        pass
