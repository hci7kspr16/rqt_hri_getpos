from python_qt_binding.QtGui import *
from python_qt_binding.QtCore import *
 
class ObjectIcon(QGraphicsPixmapItem):
    def __init__(self, filePath):
        thePM = QPixmap(filePath)
        QGraphicsPixmapItem.__init__(thePM.scaled(100,100))
        self._selected = False
        
    def selected(self, m_sel):
        self._selected = m_sel
        
    def mouseReleaseEvent(self, ev):
        self.emit(SIGNAL('clicked()'))
        
    def paint(self, qp, options, widget):
        if self._selected:
            qp.setBrush(QBrush(Qt.black)) #light transparent version of the text color
            qp.drawLine(0,0,100,100)
            
        QGraphicsPixmapItem.paint(self,qp, options, widget)
